/* EID project 2018 */
/*
    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) -
        Echo: Echo (OUTPUT) -
        GND: GND
*/

/* Pins used:
    11, 12: Sensor 1 (trig, echo)
    9, 10: Sensor 2 (trig, echo)
    2: Servo 1
    3: Servo 2
    4: Green Led
    5: Red Led
*/
/*------------------------------------------------------------------*/

#include <Servo.h>
#include <NewPing.h>
#define pingSpeed 100             // Ping frequency (in milliseconds), fastest we should ping is about 35ms per sensor

//Servo class:
Servo Lock1;
Servo Lock2;

//Ultrasonic Sensor class:
NewPing sonar1(11, 12, 200);      // Sensor 1: trigger pin, echo pin, maximum distance in cm
NewPing sonar2(9, 10, 200);       // Sensor 2: same stuff

//Servo declarations:
const int servo1pin = 2;          //1st Servo lock
const int servo2pin = 3;          //2nd Servo lock
const int openDoor = 0;           //Servo angle to allow throwing of rubbish
const int closeDoor = 60;         //Servo angle to disallow throwing of rubbish

//Ultrasonic Sensors declaration:
unsigned long pingTimer1, pingTimer2;
float in1;                        //Dist dectected by first sensor
float in2;                        //Dist detected by second sensor
float AvgDistance = 0;            //Avg. Dist of rubbish level
float maxRubbishHeight = 0.30;    //EXPERIMENT WITH THIS VALUE

//LED declarations:
const int GreenLEDpin = 4;
const int RedLEDpin = 5;

//Variables state:
bool GreenLEDstate = false;        //false for light up
bool RedLEDstate = true;

//To do sampling:
const int numReadings = 10;       //Average how many samples of readings
float readings[numReadings];      // the readings from the analog input
int readIndex = 0;                // the index of the current reading
float total = 0;                  // the running total
float average = 0;                // the average

void setup()
{
  pingTimer1 = millis() + pingSpeed;         // Sensor 1 fires after 100ms (pingSpeed)
  pingTimer2 = pingTimer1 + (pingSpeed / 2); // Sensor 2 fires 50ms later

  pinMode(GreenLEDpin, OUTPUT);
  pinMode(RedLEDpin, OUTPUT);

  Lock1.attach(servo1pin);
  Lock2.attach(servo2pin);

  //Initial set-up:
  Lock1.write(openDoor);
  Lock2.write(openDoor);

  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++)
  {
    readings[thisReading] = 0;
  }

  Serial.begin(9600);
}

void loop()
{
  if (millis() >= pingTimer1)
  {
    pingTimer1 += pingSpeed; // Make sensor 1 fire again 100ms later (pingSpeed)
    in1 = (sonar1.ping_cm()) / (float)100;
  }

  if (millis() >= pingTimer2)
  {
    pingTimer2 = pingTimer1 + (pingSpeed / 2); // Make sensor 2 fire again 50ms after sensor 1 fires
    in2 = (sonar2.ping_cm()) / (float)100;

  }
  AvgDistance = (in1 + in2) / 2;

  // subtract the last reading:
  total = total - readings[readIndex];

  // read from the sensor:
  readings[readIndex] = AvgDistance;

  // add the reading to the total:
  total = total + readings[readIndex];

  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings)
  {
    // ...wrap around to the beginning:
    readIndex = 0;
  }

  // calculate the average:
  average = total / numReadings;
  // send it to the computer as ASCII digits

  if (average >= maxRubbishHeight)
  {
    Lock1.write(openDoor);
    Lock2.write(openDoor);        //Lock the rubbish bin flap
    GreenLEDstate = false;        //Turn on Green LED (Indicates availability)
    RedLEDstate = true;           //Turn off Red LED
  }
  else
  {
    Lock1.write(closeDoor);
    Lock2.write(closeDoor);       //Unlock the rubbish bin flap
    GreenLEDstate = true;         //Turn off Green LED
    RedLEDstate = false;          //Turn on Red LED (Indicates unavailability)
  }


  Serial.print("Distance 1 = ");
  Serial.print(in1, 5);
  Serial.println(" m");

  Serial.print("Distance 2 = ");
  Serial.print(in2, 5);
  Serial.println(" m");

  Serial.print("Average Distance = ");
  Serial.print(AvgDistance, 5);
  Serial.println(" m");

  Serial.print("Average of 10 readings = ");
  Serial.print(average);
  Serial.println(" m");
  Serial.println("");

  digitalWrite(GreenLEDpin, GreenLEDstate);
  digitalWrite(RedLEDpin, RedLEDstate);
  delay(500);
}
