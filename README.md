# NTU - [MAE Engineering Innovation and Design 2018](https://eps.ntu.edu.sg/client/en_US/maeeid/?rm=MAEEID+EXHIBIT1%7C%7C%7C1%7C%7C%7C0%7C%7C%7Ctrue)

This is a compulsory module (2AU) that all MAE students have to go through which spans across entire year 2 of university. 

## Data Enabled Smart Bin (DESB)

Together with 6 other group mates, we designed and fabricated this Smart Bin which uses [Ultrasonic sensors](https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/) to track rubbish level inside the bin 
and locks the bin's opening lid using [servo motors](https://www.instructables.com/id/Arduino-Servo-Motors/) when it is full.

The rubbish level data collected will then be transmitted via bluetooth to the rubbish collector, notifying him to empty the bin. \
(This part of the code is left out in this repo) 

My role in this project is to code out the interfaces, using Arduino Uno, and making sure that each component work smoothly with each other as intended. 

For our innovation, we were selected out of the 74 projects in total by NTU Libraries to be on display in Lee Wee Nam Library (Northspine). \
More details can be found [here](https://eps.ntu.edu.sg/client/en_US/maeeid/search/detailnonmodal/ent:$002f$002fSD_ASSET$002f0$002f53336/one?qu=Data+Enabled+Smart+Bin&te=ASSET).

![My Group Mates](Images/GroupPic.jpg) 

**Our EID Poster:**
![EID Poster](Images/DESB_Poster.png)
